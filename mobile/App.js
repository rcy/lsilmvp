import React, { useState, useEffect, useRef } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';

export default function App() {
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const cameraRef = useRef(null);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  async function snap(camera) {
    console.log('snap')
    const photo = await camera.takePictureAsync({
      base64: true,
      exif: true,
    })
    console.log({ ...photo, base64: photo.base64.slice(0,100) })

    const data = photo.base64.split(',').pop() // on web this a data uri, on android it is just base64 image data

    const result = await fetch('https://lsil-backend.ngrok.io/notes.json', {
      method: 'post',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        note: {
          data
        }
      })
    })
  }

  return (
    <View style={styles.container}>
      <Camera
        style={styles.camera}
        type={type}
        ref={cameraRef}
      >
      </Camera>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            snap(cameraRef.current)
          }}>
          <Text style={styles.text}> Shoot </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  camera: {
    flexBasis: 1,
    flexShrink: 0,
    flexGrow: 1,
    transform: Platform.OS === 'web' ? 'scaleX(-1)' : [],
  },
  buttonContainer: {
    flexBasis: 'auto',
    flexShrink: 1,
    flexGrow: 0,
    backgroundColor: 'black',
    flexDirection: 'row',
    margin: 0,
  },
  button: {
    flex: 1,
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
});
