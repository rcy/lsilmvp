with (import <nixpkgs> {});
mkShell {
  buildInputs = [
    sqlite
    ruby
    postgresql_13
  ];

  shellHook = ''
    mkdir -p .nix-gems
    export GEM_HOME=$PWD/.nix-gems
    export GEM_PATH=$GEM_HOME
    export PATH=$GEM_HOME/bin:$PATH
    export PATH=$PWD/bin:$PATH
  '';
}
