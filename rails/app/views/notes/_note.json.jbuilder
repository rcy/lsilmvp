json.extract! note, :id, :comment, :processed, :created_at, :updated_at
json.url note_url(note, format: :json)
json.photo_url rails_blob_url(note.photo)
